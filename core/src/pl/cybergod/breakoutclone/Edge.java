package pl.cybergod.breakoutclone;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

public class Edge extends Object {

    private boolean isFatal;

    public Edge(World world, Vector2 position, Vector2 begin, Vector2 end, boolean isFatal){
        super();
        this.isFatal = isFatal;
        InitAsEdgeBody(world,position,begin,end,BodyDef.BodyType.KinematicBody,
                (short)1,(short)1,false, 0.4f,0.1f,false);
        body.setUserData(this);

    }

    @Override
    public void onInit() {

    }

    @Override
    public void onEvent() {

    }

    @Override
    public void onDraw(SpriteBatch batch) {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onCollisionBegin(Object other) {

    }

    @Override
    public void onCollisionEnd(Object other) {

    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void dispose() {

    }

    public boolean getIsFatal() {
        return isFatal;
    }

    public void setIsFatal(boolean b){
        isFatal = b;
    }
}
