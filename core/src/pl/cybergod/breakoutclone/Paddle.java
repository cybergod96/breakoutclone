package pl.cybergod.breakoutclone;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;

public class Paddle extends Object implements Disposable {

    private Texture texture;

    public Paddle(World world, Vector2 position) {
        super();
        texture = new Texture(Gdx.files.local("paddle.png"));

        InitAsRectangularBody(world,new Rectangle(position.x,position.y,
                texture.getWidth(), texture.getHeight()), BodyDef.BodyType.KinematicBody,
                (short)1, (short)1, false, 0.4f, 0.1f, false);
        body.setUserData(this);

    }

    @Override
    public void onInit() {
        body.setUserData(this);
    }

    @Override
    public void onEvent() {
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT) && body.getLinearVelocity().x >= -1.3f){
            //body.applyLinearImpulse(new Vector2(-10/128.f, 0),body.getWorldCenter(),true);
            body.setLinearVelocity(new Vector2(-6,0));
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)&& body.getLinearVelocity().x <= 1.3f){
            //body.applyLinearImpulse(new Vector2(10/128.f, 0),body.getWorldCenter(),true);
            body.setLinearVelocity(new Vector2(6,0));
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.S)){
            body.setLinearVelocity(new Vector2(0,0));
        }
    }

    @Override
    public void onDraw(SpriteBatch batch) {
        batch.draw(texture,body.getPosition().x*128.f - texture.getWidth()/2,body.getPosition().y*128.f - texture.getHeight()/2);
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onCollisionBegin(Object other) {

    }

    @Override
    public void onCollisionEnd(Object other) {

    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void dispose() {
        texture.dispose();
    }
}
