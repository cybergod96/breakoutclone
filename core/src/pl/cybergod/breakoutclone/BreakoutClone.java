package pl.cybergod.breakoutclone;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class BreakoutClone extends ApplicationAdapter {
    SpriteBatch batch;
    //Box2DDebugRenderer debugRenderer;
    OrthographicCamera camera;
    Viewport viewport;

    @Override
    public void create() {
        batch = new SpriteBatch();

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800/128.f, 600/128.f);
        //debugRenderer = new Box2DDebugRenderer();
        viewport = new FitViewport(800 / 128.f,600 / 128.f, camera);

        StateMachine.getInstance().RegisterState(StateIds.STATE_MENU, new MenuState());
        StateMachine.getInstance().RegisterState(StateIds.STATE_GAMEPLAY, new GameplayState());
        StateMachine.getInstance().RegisterState(StateIds.STATE_HELP, new HelpState());
        StateMachine.getInstance().RegisterState(StateIds.STATE_GAMEOVER, new GameoverState());
        StateMachine.getInstance().PushState(StateIds.STATE_MENU);
    }

    @Override
    public void render() {

        if(StateMachine.getInstance().CanExit())
            Gdx.app.exit();

        StateMachine.getInstance().OnEvent();

        StateMachine.getInstance().OnUpdate();

        camera.update();


        Gdx.gl.glClearColor(0, 0, 0, 1);
        //Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));

        //debugRenderer.render(((GameplayState)(StateMachine.getInstance().GetRegisteredState(StateIds.STATE_GAMEPLAY))).getWorld(),camera.combined);


        //batch.enableBlending();
        //batch.setProjectionMatrix(camera.combined);
        batch.begin();
        //batch.draw(img, 10, 0);
        StateMachine.getInstance().OnDraw(batch);
        batch.end();
    }

    @Override
    public void dispose() {
        batch.dispose();
        //debugRenderer.dispose();
        StateMachine.getInstance().dispose();
    }
}
