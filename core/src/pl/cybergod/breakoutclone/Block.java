package pl.cybergod.breakoutclone;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

public class Block extends Object {

    Texture texture;

    public Block(World world, Vector2 position){
        super();
        texture = new Texture(Gdx.files.local("block.png"));
        InitAsRectangularBody(world,new Rectangle(position.x,position.y,
                texture.getWidth(), texture.getHeight()), BodyDef.BodyType.StaticBody,
                (short)1, (short)1, false, 0.2f, 0.0f, false);
        body.setUserData(this);

    }

    @Override
    public void onInit() {
        body.setUserData(this);
    }

    @Override
    public void onEvent() {

    }

    @Override
    public void onDraw(SpriteBatch batch) {
        batch.draw(texture, body.getPosition().x * 128.f - texture.getWidth() / 2, body.getPosition().y * 128.f - texture.getHeight() / 2);
    }

    @Override
    public void onDestroy() {
        body.getWorld().destroyBody(body);

    }

    @Override
    public void onCollisionBegin(Object other) {

    }

    @Override
    public void onCollisionEnd(Object other) {

    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void dispose() {
        texture.dispose();
    }
}
