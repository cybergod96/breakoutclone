package pl.cybergod.breakoutclone;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

class MenuOption implements Disposable {
    private String name;
    private boolean isSelected;
    private BitmapFont font;
    private float scale;

    public MenuOption(String name, boolean isSelected, BitmapFont font, float scale) {
        this.name = name;
        this.font = font;
        this.font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        setSelected(isSelected);
    }

    public void setScale(float scale) {
        this.scale = scale;
        font.getData().setScale(this.scale);
    }

    public void setSelected(boolean b) {
        isSelected = b;
        font.setColor(isSelected ? Color.WHITE : Color.GREEN);
    }

    public void draw(SpriteBatch batch, Vector2 pos) {
        font.draw(batch, name, pos.x, pos.y);
    }

    @Override
    public void dispose() {
        font.dispose();
    }
}

public class MenuState extends State {

    private final int numOptions = 3;
    private int selectedOption;
    private MenuOption[] options;

    public MenuState() {
        super();

        options = new MenuOption[3];
        options[0] = new MenuOption("New Game", true, new BitmapFont(), 3);
        options[1] = new MenuOption("Help", true, new BitmapFont(), 3);
        options[2] = new MenuOption("Exit", true, new BitmapFont(), 3);
    }

    @Override
    public void OnEnter(int previousStateId) {
        selectedOption = 0;
    }

    @Override
    public void OnExit(int nextStateId) {

    }

    @Override
    public void OnSuspend(int pushedStateId) {

    }

    @Override
    public void OnResume(int poppedStateId) {

    }

    @Override
    public void OnDraw(SpriteBatch target, boolean suspended) {
        for (int i = 0; i < numOptions; i++) {
            options[i].draw(target, new Vector2(380, 500 - i * 64));
        }
    }

    @Override
    public void OnEvent(boolean suspended) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            switch (selectedOption) {
                case 0:
                    StateMachine.getInstance().PopState();
                    StateMachine.getInstance().PushState(StateIds.STATE_GAMEPLAY);
                    break;
                case 1:
                    StateMachine.getInstance().PopState();
                    StateMachine.getInstance().PushState(StateIds.STATE_HELP);
                    break;
                case 2:
                    StateMachine.getInstance().PopAllStates();
                    break;
                default:
                    break;
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            selectedOption = ((selectedOption - 1)+numOptions) % numOptions;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            selectedOption = (selectedOption + 1) % numOptions;
        }
    }

    @Override
    public void OnUpdate(boolean suspended) {
        for (int i = 0; i < numOptions; i++) {
            options[i].setSelected(i == selectedOption);
        }

    }

    @Override
    public void OnNotify(String notification) {

    }

    @Override
    public void dispose() {
        for (int i = 0; i < numOptions; i++) {
            options[i].dispose();
        }
    }
}
