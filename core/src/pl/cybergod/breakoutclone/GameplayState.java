package pl.cybergod.breakoutclone;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class GameplayState extends State {

    private final int rows = 6;
    private final int columns = 12;

    int points, numBlocks;

    BitmapFont font;

    ObjectManager objectManager;

    World world;

    public GameplayState(){
        world = new World(new Vector2(0.f,0.f),true);
        world.setContactListener(new MyContactListener());

        objectManager = new ObjectManager();

        objectManager.Insert(new Paddle(world, new Vector2(400 - 48, 0)));
        objectManager.Insert(new Ball(world, new Vector2(400 - 8, 32)));
        objectManager.Insert(new Edge(world,new Vector2(0,0),new Vector2(0,0), new Vector2(0,600),false));
        objectManager.Insert(new Edge(world,new Vector2(0,0),new Vector2(800,0), new Vector2(800,600),false));
        objectManager.Insert(new Edge(world,new Vector2(0,0),new Vector2(0,600), new Vector2(800,600),false));
        objectManager.Insert(new Edge(world,new Vector2(0,-16),new Vector2(0,-16), new Vector2(800,-16),true));

        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                objectManager.Insert(new Block(world,new Vector2(200+j*32,500 - i*16)));
            }
        }

        font = new BitmapFont();
        font.getData().setScale(1.5f);
        font.setColor(Color.WHITE);
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear,Texture.TextureFilter.Linear);
    }

    @Override
    public void OnEnter(int previousStateId) {
        points = 0;
    }

    @Override
    public void OnExit(int nextStateId) {

    }

    @Override
    public void OnSuspend(int pushedStateId) {

    }

    @Override
    public void OnResume(int poppedStateId) {

    }

    @Override
    public void OnDraw(SpriteBatch target, boolean suspended) {
        objectManager.OnDraw(target);
        font.draw(target,"Points: "+points, 0,560);
    }

    @Override
    public void OnEvent(boolean suspended) {
        objectManager.OnEvent();
    }

    @Override
    public void OnUpdate(boolean suspended) {
        world.step(1/60.f, 8, 3);
        objectManager.OnUpdate();
    }

    @Override
    public void OnNotify(String notification) {
        if(notification == "BlockDestroyed"){
            points ++;

        }
        else if(notification == "BallDestroyed"){
            StateMachine.getInstance().PopState();
            StateMachine.getInstance().PushState(StateIds.STATE_GAMEOVER);
        }
    }

    @Override
    public void dispose() {
        world.dispose();
        objectManager.dispose();
        font.dispose();
    }
}
