package pl.cybergod.breakoutclone;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

public abstract class State implements Disposable {
    public abstract void OnEnter(int previousStateId);

    public abstract void OnExit(int nextStateId);

    public abstract void OnSuspend(int pushedStateId);

    public abstract void OnResume(int poppedStateId);

    public abstract void OnDraw(SpriteBatch target, boolean suspended);

    public abstract void OnEvent(boolean suspended);

    public abstract void OnUpdate(boolean suspended);

    public abstract void OnNotify(String notification);
}
