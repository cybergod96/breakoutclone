package pl.cybergod.breakoutclone;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameoverState extends State {

    BitmapFont font;

    public GameoverState(){
        font = new BitmapFont();
        font.setColor(Color.WHITE);
        font.getData().setScale(4);
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear,Texture.TextureFilter.Linear);
    }

    @Override
    public void OnEnter(int previousStateId) {

    }

    @Override
    public void OnExit(int nextStateId) {

    }

    @Override
    public void OnSuspend(int pushedStateId) {

    }

    @Override
    public void OnResume(int poppedStateId) {

    }

    @Override
    public void OnDraw(SpriteBatch target, boolean suspended) {
        font.draw(target, "GAME OVER\nPress ENTER",100,300);
    }

    @Override
    public void OnEvent(boolean suspended) {
        if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER)){
            StateMachine.getInstance().PopState();
            StateMachine.getInstance().PushState(StateIds.STATE_MENU);
        }
    }

    @Override
    public void OnUpdate(boolean suspended) {

    }

    @Override
    public void OnNotify(String notification) {

    }

    @Override
    public void dispose() {
        font.dispose();
    }
}
