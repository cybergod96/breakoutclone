package pl.cybergod.breakoutclone;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;

public class ObjectManager implements Disposable {
    private ArrayList<Object> objects;

    public ObjectManager(){
        objects = new ArrayList<Object>();
    }

    public void Insert(Object o){
        o.onInit();
        objects.add(o);
    }

    public void OnUpdate(){
        for(int i = objects.size() - 1; i >= 0; i--){
            if(objects.get(i).getCanDestroy()){
                objects.get(i).onDestroy();
                objects.get(i).dispose();
                objects.remove(i);
            }
        }
        for(Object o : objects){
            o.onUpdate();
        }
    }

    public void OnEvent(){
        for(Object o : objects){
            o.onEvent();
        }
    }

    public void OnDraw(SpriteBatch batch){
        for(Object o : objects){
            o.onDraw(batch);
        }
    }

    @Override
    public void dispose() {
        for(Object o : objects){
            o.dispose();
        }
    }
}
