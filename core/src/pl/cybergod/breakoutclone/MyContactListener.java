package pl.cybergod.breakoutclone;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class MyContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        if(contact.getFixtureA().getBody().getUserData() == null ||
                contact.getFixtureB().getBody().getUserData() == null)
            return;

        Object a = (Object)contact.getFixtureA().getBody().getUserData();
        Object b = (Object)contact.getFixtureB().getBody().getUserData();

        a.onCollisionBegin(b);
        b.onCollisionBegin(a);

        //Gdx.app.log("Info", "Collision begin");
    }

    @Override
    public void endContact(Contact contact) {
        if(contact.getFixtureA().getBody().getUserData() == null ||
                contact.getFixtureB().getBody().getUserData() == null)
            return;

        Object a = (Object)contact.getFixtureA().getBody().getUserData();
        Object b = (Object)contact.getFixtureB().getBody().getUserData();

        a.onCollisionEnd(b);
        b.onCollisionBegin(a);

        //Gdx.app.log("Info", "Collision end");
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
