package pl.cybergod.breakoutclone;

public class StateIds {
    public static final int STATE_MENU = 0;
    public static final int STATE_GAMEPLAY = 1;
    public static final int STATE_GAMEOVER = 2;
    public static final int STATE_HELP = 3;
}
