package pl.cybergod.breakoutclone;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Disposable;

public abstract class Object implements Disposable {
    protected Body body;
    protected boolean canDestroy;
    protected Vector2 size;

    public Object() {
        canDestroy = false;
        size = new Vector2(0,0);
    }

    public abstract void onInit();

    public abstract void onEvent();

    public abstract void onDraw(SpriteBatch batch);

    public abstract void onDestroy();

    public abstract void onCollisionBegin(Object other);

    public abstract void onCollisionEnd(Object other);

    public abstract void onUpdate();

    public void setCanDestroy(boolean canDestroy) {
        this.canDestroy = canDestroy;
    }

    public boolean getCanDestroy() {
        return canDestroy;
    }

    public Body getBody() {
        return body;
    }

    /**
     * friction = 0.2f
     * restitution = 0.0f
     *
     */
    public void InitAsRectangularBody(World world, Rectangle rect, BodyDef.BodyType type, short category, short mask,
                                      boolean originBasedPosition, float friction, float restitution, boolean isSensor) {
        Vector2 bodyPosition = new Vector2();
        BodyDef bodyDef = new BodyDef();

        size.set(rect.width/128.f, rect.height/128.f);

        if (!originBasedPosition) {
            bodyPosition.x = (rect.x + rect.width / 2) / 128.f;
            bodyPosition.y = (rect.y + rect.height / 2) / 128.f;
        } else {
            bodyPosition.x = rect.x / 128.f;
            bodyPosition.y = rect.y / 128.f;
        }

        bodyDef.position.set(bodyPosition);
        bodyDef.type = type;
        body = world.createBody(bodyDef);

        PolygonShape bodyShape = new PolygonShape();
        bodyShape.setAsBox(rect.width / 2 / 128.f, rect.height / 2 / 128.f);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;
        fixtureDef.isSensor = isSensor;
        fixtureDef.shape = bodyShape;
        fixtureDef.filter.categoryBits = category;
        fixtureDef.filter.maskBits = mask;
        body.createFixture(fixtureDef);
    }

    public void InitAsCircularBody(World world, Vector2 position, float radius, BodyDef.BodyType type, short category,
                                   short mask, boolean originBasedPosition, float friction, float restitution,
                                   boolean isSensor){
        BodyDef bodyDef = new BodyDef();

        size.set(radius*2/128.f,radius*2/128.f);

        bodyDef.position.set(position.x/128.f,position.y/128.f);
        bodyDef.type = type;

        body = world.createBody(bodyDef);

        CircleShape shape = new CircleShape();
        shape.setRadius(radius/128.f);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.friction = friction;
        fixtureDef.restitution = restitution;
        fixtureDef.isSensor = isSensor;
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = category;
        fixtureDef.filter.maskBits = mask;
        body.createFixture(fixtureDef);

    }

    public void InitAsEdgeBody(World world, Vector2 position, Vector2 begin, Vector2 end, BodyDef.BodyType type, short category,
                               short mask, boolean originBasedPosition, float friction, float restitution,
                               boolean isSensor){
            BodyDef bodyDef = new BodyDef();
            bodyDef.position.set(position.x/128.f,position.y/128.f);
            bodyDef.type = type;

            body = world.createBody(bodyDef);

            EdgeShape shape = new EdgeShape();

            shape.set(begin.scl(1/128.f),end.scl(1/128.f));

            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.friction = friction;
            fixtureDef.restitution = restitution;
            fixtureDef.isSensor = isSensor;
            fixtureDef.shape = shape;
            fixtureDef.filter.categoryBits = category;
            fixtureDef.filter.maskBits = mask;
            body.createFixture(fixtureDef);
    }

    public Vector2 GetPositionPixels(){
        return new Vector2(body.getPosition().x*128.f, body.getPosition().y*128.f);
    }

    public Vector2 GetPositionMeters(){
        return body.getPosition();
    }

    public Vector2 GetSizePixels(){
        return new Vector2(size.x*128.f, size.y*128.f);
    }

    public Vector2 GetSizeMeters(){
        return size;
    }
}
