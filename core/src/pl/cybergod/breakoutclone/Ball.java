package pl.cybergod.breakoutclone;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;

public class Ball extends Object {
    private Texture texture;

    public Ball(World world, Vector2 position) {
        super();
        texture = new Texture(Gdx.files.local("ball.png"));

        InitAsCircularBody(world, new Vector2(position.x, position.y), texture.getWidth()/2, BodyDef.BodyType.DynamicBody,
                (short) 1, (short) 1, false, 0.f, 1.0f, false);
        body.setUserData(this);

    }

    @Override
    public void onInit() {
        body.setUserData(this);
    }

    @Override
    public void onEvent() {
        if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
            body.applyLinearImpulse(new Vector2(0.5f,2f),body.getWorldCenter(),true);
        }

    }

    @Override
    public void onDraw(SpriteBatch batch) {
        batch.draw(texture, body.getPosition().x * 128.f - texture.getWidth() / 2, body.getPosition().y * 128.f - texture.getHeight() / 2);
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onCollisionBegin(Object other) {
        if(other instanceof Edge){
            Edge e = (Edge)other;
            if(e.getIsFatal()) {
                StateMachine.getInstance().NotifyState(StateIds.STATE_GAMEPLAY, "BallDestroyed");
            }
        }

        if(other instanceof Block){
            other.setCanDestroy(true);
            StateMachine.getInstance().NotifyState(StateIds.STATE_GAMEPLAY, "BlockDestroyed");
        }
    }

    @Override
    public void onCollisionEnd(Object other) {

    }

    @Override
    public void onUpdate() {

    }

    @Override
    public void dispose() {
        texture.dispose();
    }
}
