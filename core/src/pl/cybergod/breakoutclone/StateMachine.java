package pl.cybergod.breakoutclone;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class StateMachine implements Disposable {
    private LinkedList<State> states;
    private HashMap<Integer, State> registeredStates;
    private static StateMachine instance = new StateMachine();

    private StateMachine() {
        states = new LinkedList<State>();
        registeredStates = new HashMap<Integer, State>();
    }

    public void RegisterState(Integer id, State state) {
        if (!IsRegistered(id) && !IsRegistered(state)) {
            registeredStates.put(id, state);
        }
    }

    public boolean IsRegistered(Integer id) {
        return registeredStates.containsKey(id);
    }

    public boolean IsRegistered(State state) {
        return registeredStates.containsValue(state);
    }

    public void PushState(Integer id) {
        if (!IsRegistered(id)) return;
        if (states.size() > 0) states.getFirst().OnSuspend(id);

        State newState = GetRegisteredState(id);
        Integer previousStateId = -1;

        if (!states.isEmpty()) previousStateId = StateId(states.getFirst());

        states.addFirst(newState);
        newState.OnEnter(previousStateId);
    }

    public void PopState() {
        if (states.isEmpty()) return;
        State poppedState = states.getFirst();
        Integer poppedStateId = StateId(poppedState);
        Integer resumedStateId = (states.size() > 1) ? StateId(states.get(1)) : -1;
        poppedState.OnExit(resumedStateId);
        states.removeFirst();
        if (states.size() > 0) states.getFirst().OnResume(poppedStateId);
    }

    public Integer StateId(State state) {
        for (int i = 0; i < registeredStates.size(); i++) {
            if (registeredStates.get(i) == state) {
                return i;
            }
        }
        return -1;
    }

    public State GetRegisteredState(Integer id) {
        return registeredStates.get(id);
    }

    public void PopAllStates() {
        states.clear();
    }

    public void OnUpdate() {
        if (states.isEmpty()) return;
        states.getFirst().OnUpdate(false);

        for (int i = 1; i < states.size(); i++) {
            states.get(i).OnUpdate(true);
        }
    }

    public void OnDraw(SpriteBatch target) {
        if (states.isEmpty()) return;
        for (int i = 1; i < states.size(); i++) {
            states.get(i).OnDraw(target, true);
        }

        states.getFirst().OnDraw(target, false);
    }

    public void OnEvent() {
        if (states.isEmpty()) return;

        states.getFirst().OnEvent( false);
        for (int i = 1; i < states.size(); i++) {
            states.get(i).OnEvent(true);
        }
    }

    public boolean CanExit() {
        return states.isEmpty();
    }

    public void NotifyState(Integer id, String notification){
        if(!IsRegistered(id)) return;
        registeredStates.get(id).OnNotify(notification);
    }

    public static StateMachine getInstance() {
        return instance;
    }

    @Override
    public void dispose() {
        Iterator it = registeredStates.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            ((State)pair.getValue()).dispose();
        }
    }
}
